#!/bin/bash
file="$(pwd)/gitlib.txt"

apt-get update
cd ~
 
# Install arduino-cli
apt-get install curl -y
curl -L -o arduino-cli.tar.bz2 https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2
tar xjf arduino-cli.tar.bz2
rm arduino-cli.tar.bz2
mv `ls -1` /usr/bin/arduino-cli
 
# # Install python, pip and pyserial
# apt-get install python3 -y
# curl https://bootstrap.pypa.io/pip/3.6/get-pip.py -o get-pip.py
# python3 get-pip.py
pip install pyserial
 
# Install esp32 core
printf "board_manager:\n  additional_urls:\n    - https://dl.espressif.com/dl/package_esp32_index.json\n" > .arduino-cli.yaml
arduino-cli core update-index --config-file .arduino-cli.yaml
arduino-cli core install esp32:esp32 --config-file .arduino-cli.yaml
 
# Install 'native' packages

for LIB in $NATIVEARDUINOLIB
do
    arduino-cli lib install $($LIB)
done

cd -
 
# Install 'third-party' packages: find proper location and 'git clone'
apt-get install git -y
cd `arduino-cli config dump | grep sketchbook | sed 's/.*\ //'`/libraries

while IFS= read line
do
    git clone "$line"
done <"$file"

cd -
