<div align='center'>
    <h1><b> Gitlab CI Templates </b></h1>
    <p>A Collection of CI & CD Templates to be used on my projects..</p>
</div>


---

## ⚙️ **Configuration**


<br/>

---

## 💻 **TECHNOLOGIES**

![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white)
![NPM](https://img.shields.io/badge/NPM-%23000000.svg?style=for-the-badge&logo=npm&logoColor=white)
![SASS](https://img.shields.io/badge/SASS-hotpink.svg?style=for-the-badge&logo=SASS&logoColor=white)

<br />

---

## 📎 **LICENSE**

Copyright 2023 Marley Plant

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

<br />

---

## 📌 **LINKS**

[<img alt="Gitlab" src="https://img.shields.io/badge/MarleyPlant-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white" />](https://gitlab.com/MarleyPlant)

<br />

---

## 📁 **Related Projects**

